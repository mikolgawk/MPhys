# AI approach to materials with flat electronic bands hosting exotic physics

## MPhys Project by Mikolaj Gawkowski and Mateusz Czajka
## Supervised by Anupam Bhattacharya and Artem Mishchenko, The University of Manchester

## Project description
Semester 1: The goal of the project is to identify flat bands in electronic structures of 3D materials.
A convolutional neural network trained using Matlab is used to identify flat segments in band structures.
Semester 2: We want to use the band structure and density of state fingerpints to reconstruct the DFT charge density outputs. We will utilize both deep neural netowrks as well as GNNs in the project.

## Files
The main part of the code is contained in the `Classification_main.py`.




